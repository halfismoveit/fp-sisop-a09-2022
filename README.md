# fp-sisop-A09-2022

Praktikum Final Sistem Operasi 2022 Kelompok A09

## Objektif
Membuat database sederhana dengan menerapkan server-client. Program menggunakan bahasa C. Ketentuan lebih lanjut mengenai database berlanjut di bawah.
### Gambaran program server
Pertama, diperlukan definisi library yang akan digunakan dan port. Kemudian, langsung ke fungsi utama.
```
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#define PORT 8080
```
Di fungsi utama, pertama perlu dideklarasikan variabel-variabel yang akan digunakan.
```
int main(int argc, char const *argv[])
{
 int server_fd, new_socket, valread;
 struct sockaddr_in address;
 int opt = 1;
 int addrlen = sizeof(address);
 char buffer[1024] = {0};
 char jumlah[50];
 int numbers[100];
 char dblist[100][100];
```
Kemudian, kode untuk menghubungkan dengan client.
```
 if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
 {
   perror("socket failed");
   exit(EXIT_FAILURE);
 }
 
 if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
 {
   perror("setsockopt");
   exit(EXIT_FAILURE);
 }
 
 address.sin_family = AF_INET;
 address.sin_addr.s_addr = INADDR_ANY;
 address.sin_port = htons(PORT);
 
 if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0)
 {
   perror("bind failed");
   exit(EXIT_FAILURE);
 }
 
 if (listen(server_fd, 3) < 0)
 {
   perror("listen");
   exit(EXIT_FAILURE);
 }
 
 if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
 {
   perror("accept");
   exit(EXIT_FAILURE);
 }
```
Kemudian, kode untuk menerima input dari client dan melakukan hal tertentu sesuai input. Jika ada command yang tidak sesuai, akan muncul pesan error tanpa mengeluarkan user dari program.
```
 int index = 0;
 while (1)
 {
   valread = read(new_socket, buffer, 1024);
 
   if (strcmp(buffer, "SHOW_DATABASES") == 0)
   {
     char response[1024];
     int strIndex = 0;
     for (int i = 0; i < index; i++)
     {
       strIndex += snprintf(&response[strIndex], 1024 - strIndex, "%d ", numbers[i]);
     }
     if (index == 0)
     {
       char *msg = "No databases created";
       send(new_socket, msg, strlen(msg), 0);
     }
     else
     {
       send(new_socket, response, strlen(response), 0);
     }
   }
   else if (strcmp(buffer, "CREATE_DATABASE") == 0)
   {
     memset(buffer, 0, 1024);
     valread = read(new_socket, buffer, 1024);
 
     int num = atoi(buffer);
     numbers[index] = num;
     printf("%d\n", num);
     index++;
 
     char *msg = "Database created";
 
     send(new_socket, msg, strlen(msg), 0);
   }
   
   else if (strcmp(buffer, "USE") == 0)
   {
     memset(buffer, 0, 1024);
     valread = read(new_socket, buffer, 1024);
 
 
     char *msg = "Database changed";
 
     send(new_socket, msg, strlen(msg), 0);
   }
   else if (strcmp(buffer, "DROP_DATABASE") == 0)
   {
     memset(buffer, 0, 1024);
     valread = read(new_socket, buffer, 1024);


     char *msg = "Database dropped";
 
     send(new_socket, msg, strlen(msg), 0);
   }

   else
   {
     char *msg = "Error";
     send(new_socket, msg, strlen(msg), 0);
   }
 
   memset(buffer, 0, 1024);
 }
 return 0;
}
```
### Gambaran program client
Pertama, definisi library yang akan digunakan dan portnya. Kemudian, ke fungsi utama.
```
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#define PORT 8080
```
Di awal fungsi utama, didefinisikan variabel yang akan digunakan.
```
int main(int argc, char const *argv[])
{
 struct sockaddr_in address;
 int sock = 0, valread;
 struct sockaddr_in serv_addr;
 char *hello = "Hello from client";
 char buffer[1024] = {0};
```
Kemudian kode untuk menghubungkan dengan server.
```
 if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
 {
   printf("\n Socket creation error \n");
   return -1;
 }
 memset(&serv_addr, '0', sizeof(serv_addr));
 serv_addr.sin_family = AF_INET;
 serv_addr.sin_port = htons(PORT);
 
 if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
 {
   printf("\nInvalid address/ Address not supported \n");
   return -1;
 }
 if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
 {
   printf("\nConnection Failed \n");
   return -1;
 }
```
Selanjutnya, client dapat mulai membaca input dari user. Untuk beberapa command, diperlukan input tambahan dari user. Seperti ```CREATE_DATABASE``` dan ```USE```.
```
 while (1)
 {
   char *input;
   scanf("%s", input);
   send(sock, input, strlen(input), 0);
   
   if (strcmp(input, "CREATE_DATABASE") == 0)
   {
     scanf("%s", input);
     send(sock, input, strlen(input), 0);
   }
   else if (strcmp(input, "USE") == 0)
   {
     scanf("%s", input);
     send(sock, input, strlen(input), 0);
   }
   else if (strcmp(input, "DROP_DATABASE") == 0)
   {
     scanf("%s", input);
     send(sock, input, strlen(input), 0);
   } 
    
   valread = read(sock, buffer, 1024);
   printf("%s\n", buffer);
   memset(buffer, 0, 1024);

 }
 return 0;
}
```
### Dapat membuat database
Untuk membuat database, diperlukan input nama database dari user. Sehingga, setelah user menggunakan command ```CREATE_DATABASE```, user akan memasukkan nama database yang diinginkan. Selanjutnya, setelah menggunakan command ```CREATE_DATABASE``` dan memasukkan nama database di program client, program server akan memproses input dan menyimpan nama database tersebut.

Client:
```
   if (strcmp(input, "CREATE_DATABASE") == 0)
   {
     scanf("%s", input);
     send(sock, input, strlen(input), 0);
   }
```
Server:
```
   else if (strcmp(buffer, "CREATE_DATABASE") == 0)
   {
     memset(buffer, 0, 1024);
     valread = read(new_socket, buffer, 1024);
 
     int num = atoi(buffer);
     numbers[index] = num;
     printf("%d\n", num);
     index++;
 
     char *msg = "Database created";
 
     send(new_socket, msg, strlen(msg), 0);
   }
```
Di bawah ini adalah gambar ketika membuat database di program client.

![](imag/f1.png)

Kemudian, server akan menampung database yang telah dibuat.

![](imag/f2.png)

### Menampilkan database
Untuk menampilkan database, dapat menggunakan command ```SHOW_DATABASES```. Dan karena command tersebut tidak memerlukan input lebih lanjut, maka tidak ada kode spesifik untuk ```SHOW_DATABASES``` di program client.

![](imag/f4.png)

Server:
```
   if (strcmp(buffer, "SHOW_DATABASES") == 0)
   {
     char response[1024];
     int strIndex = 0;
     for (int i = 0; i < index; i++)
     {
       strIndex += snprintf(&response[strIndex], 1024 - strIndex, "%d ", numbers[i]);
     }
     if (index == 0)
     {
       char *msg = "No databases created";
       send(new_socket, msg, strlen(msg), 0);
     }
     else
     {
       send(new_socket, response, strlen(response), 0);
     }
   }
```
Kemudian, jika terdapat kesalahan pengetikan command, akan ditampilkan pesan error. Jika tidak ada database yang telah dibuat, akan menampilkan pesan tidak ada database yang telah dibuat.

![](imag/f3.png)

### Kekurangan
- Nama database berupa angka, perlu adanya handle nama database berupa string di program server. Salah satunya dengan menggunakan array dua dimensi.
