#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#define PORT 8080
 
int main(int argc, char const *argv[])
{
 int server_fd, new_socket, valread;
 struct sockaddr_in address;
 int opt = 1;
 int addrlen = sizeof(address);
 char buffer[1024] = {0};
 char jumlah[50];
 int numbers[100];
 char dblist[100][100];
 
 if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
 {
   perror("socket failed");
   exit(EXIT_FAILURE);
 }
 
 if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
 {
   perror("setsockopt");
   exit(EXIT_FAILURE);
 }
 
 address.sin_family = AF_INET;
 address.sin_addr.s_addr = INADDR_ANY;
 address.sin_port = htons(PORT);
 
 if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0)
 {
   perror("bind failed");
   exit(EXIT_FAILURE);
 }
 
 if (listen(server_fd, 3) < 0)
 {
   perror("listen");
   exit(EXIT_FAILURE);
 }
 
 if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
 {
   perror("accept");
   exit(EXIT_FAILURE);
 }
 
 int index = 0;
 while (1)
 {
   valread = read(new_socket, buffer, 1024);
 
   if (strcmp(buffer, "SHOW_DATABASES") == 0)
   {
     char response[1024];
     int strIndex = 0;
     for (int i = 0; i < index; i++)
     {
       strIndex += snprintf(&response[strIndex], 1024 - strIndex, "%d ", numbers[i]);
     }
     if (index == 0)
     {
       char *msg = "No databases created";
       send(new_socket, msg, strlen(msg), 0);
     }
     else
     {
       send(new_socket, response, strlen(response), 0);
     }
   }
   else if (strcmp(buffer, "CREATE_DATABASE") == 0)
   {
     memset(buffer, 0, 1024);
     valread = read(new_socket, buffer, 1024);
 
     int num = atoi(buffer);
     numbers[index] = num;
     printf("%d\n", num);
     index++;
 
     char *msg = "Database created";
 
     send(new_socket, msg, strlen(msg), 0);
   }
   
   else if (strcmp(buffer, "USE") == 0)
   {
     memset(buffer, 0, 1024);
     valread = read(new_socket, buffer, 1024);
 
 
     char *msg = "Database changed";
 
     send(new_socket, msg, strlen(msg), 0);
   }
   else if (strcmp(buffer, "DROP_DATABASE") == 0)
   {
     memset(buffer, 0, 1024);
     valread = read(new_socket, buffer, 1024);


     char *msg = "Database dropped";
 
     send(new_socket, msg, strlen(msg), 0);
   }

   else
   {
     char *msg = "Error";
     send(new_socket, msg, strlen(msg), 0);
   }
 
   memset(buffer, 0, 1024);
 }
 return 0;
}
